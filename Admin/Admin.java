package Admin;

public class Admin {
	  private String adminName;
	  private String adminPassword;

	    public Admin(String adminName, String adminPassword) {
	        this.adminName = adminName;
	        this.adminPassword = adminPassword;
	    }

	    public String getAdminName() {
	        return adminName;
	    }

	    public void setAdminName(String adminName) {
	        this.adminName = adminName;
	    }

	    public String getAdminPassword() {
	        return adminPassword;
	    }

	    public void setAdminPassword(String adminPassword) {
	        this.adminPassword = adminPassword;
	    }
	    
	    // 验证管理员登录
	    public static boolean validateAdmin(String adminName, String adminPassword) {	  
	        return adminName.equals("admin") && adminPassword.equals("123456");
	    }
}