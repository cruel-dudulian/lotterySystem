package lotterySystem;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import user.User;

public class Show {

	 //显示当前用户的彩票号数和获奖情况，
    public static void displayNumbers(List<String> numbers, User user) {
        JFrame frame = new JFrame(user.getUsername() + "'s Numbers and Winning Status");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // 设置关闭操作为销毁窗口

        JPanel panel = new JPanel(new BorderLayout());

        String[] columnNames = {"彩票号码", "获奖情况"};
         Object[][] data = new Object[numbers.size()][2];

         for (int i=0; i<numbers.size();i++){
            String[] numberInfo=numbers.get(i).split(",");
            data[i][0] ="【"+ numberInfo[0]+"】";
            if(numberInfo.length>1){
                if(numberInfo[1].equals("未中奖")){
                    data[i][1]="未中奖";
                }else{
                    data[i][1]=numberInfo[1];
                }
             }else{
                 data[i][1]="未开奖";
             }
         }

         JTable table=new JTable(data,columnNames);

         JScrollPane scrollPane=new JScrollPane(table);
         panel.add(scrollPane,BorderLayout.CENTER);

         frame.add(panel);

         frame.setSize(400, 300);
         frame.setVisible(true);
     }
    //显示所有获奖者的获奖信息
    public static void displayWinnerInfos(List<String> winnerInfos, String winningNumber) {
        JFrame dialog = new JFrame("Lottery Winners");
        dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // 设置关闭操作为销毁窗口

        JPanel panel = new JPanel(new BorderLayout());

        JLabel winningLabel = new JLabel("中奖号码：" + winningNumber);
        panel.add(winningLabel, BorderLayout.NORTH);

        String[] columnNames={"用户名","彩票号码","奖项"};
        Object[][] data=new Object[winnerInfos.size()][3];

         for(int i=0;i<winnerInfos.size();i++) {
            String[] minInfo=winnerInfos.get(i).split(",");
            data[i][0]=minInfo[0];
            data[i][1]=minInfo[1];
            data[i][2]=minInfo[2];
         }

         JTable table=new JTable(data,columnNames);

         JScrollPane scrollPane=new JScrollPane(table);
         panel.add(scrollPane,BorderLayout.CENTER);

         dialog.add(panel);

          dialog.setSize(400, 300);
          dialog.setVisible(true);
      }
     
}
