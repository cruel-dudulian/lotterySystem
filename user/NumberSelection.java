package user;

import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

public class NumberSelection {
   
   
	 public static String selectNumberRandomly() {
	        Random random = new Random();
	        StringBuilder sb = new StringBuilder();

	        for (int i = 0; i < 7; i++) {
	            int number = random.nextInt(36) + 1;
	            sb.append(String.format("%02d", number)); // 使用两位宽度格式化数字，并在前面补0
	        }

	        return sb.toString();
	    }

	   public static String selectNumber(User user, String number, List<String> numbers) {
	        double price = 2.0; // 彩票价格

	        if (user.getBalance() < price) {
	            JOptionPane.showMessageDialog(null, "余额不足，请先充值");
	            return null;
	        }

	        if (!isValidNumber(number)) {
	            JOptionPane.showMessageDialog(null, "号码不合法，请重新选择");
	            return null;
	        }

	        if (numbers.contains(number)) {
	            JOptionPane.showMessageDialog(null, "号码重复，请重新选择");
	            return null;
	        }

	        user.setBalance(user.getBalance() - price); // 扣除购买彩票的金额

	        JOptionPane.showMessageDialog(null, "选择号码成功，号码为：" + number);

	        return number;
	     }

	   public static boolean isValidNumber(String number) {
		   
		   if(!number.matches("\\d{14}"))
			   return false;
	         for (int i = 0; i < number.length(); i += 2) {
	             String subNum = number.substring(i, i + 2);
	             int numValue = Integer.parseInt(subNum);
	             if (numValue < 1 || numValue > 36) { 
	                 return false; 
	             }
	         }
	         
	         return true;
	     }
    
}