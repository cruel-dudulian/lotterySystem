package user;

import java.util.List;

import record.DataStorage;

public class User {
    private String username;
    private String password;
    private double balance;
    private String telnumber;

    public User(String username, String password,String telnumber) {
        this.username=username;
        this.password=password;
        this.telnumber=telnumber;
        this.balance=0.0;
       
    }
    public User(String username, String password,double balance,String telnumber) {
        this.username=username;
        this.password=password;
        this.telnumber=telnumber;
        this.balance=balance;
       
    }
    public User(String username, String password,double balance) {
        this.username=username;
        this.password=password;
        this.balance=balance;
    }
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelnumber() {
		return telnumber;
	}
	public void setTelnumber(String telnumber) {
		this.telnumber = telnumber;
	}


	public  void recharge(User user, double amount,List<String> userRecords) {
        user.setBalance(user.getBalance() + amount);
        DataStorage.updateBalance(user.getUsername(), user.getBalance(),userRecords);
        System.out.println("充值成功！当前余额为：" + user.getBalance());
    }
}